import React from "react";
import ReactDOM from "react-dom";
import TodoModel, { FILTER_TYPES } from "./model";
// import App from "./App";

// it('renders without crashing', () => {
//   const div = document.createElement('div');
//   ReactDOM.render(<App />, div);
//   ReactDOM.unmountComponentAtNode(div);
// });

it("reduce function", () => {
  expect([1, 2, 3, 4, 5].reduce((a, b) => a + b)).toBe(15);

  expect([1, 2, 3, 4, 5].reduce((a, b) => a + b, 10)).toBe(25);

  expect([1, 2, 3, 4, 5].reduce((a, b) => a * b, 10)).toBe(1200);
});

xit("should have todo app functionality", () => {
  const m1 = new TodoModel();

  expect(m1.items).toHaveLength(0);

  m1.addTodo("ekmek al");

  expect(m1.items).toHaveLength(1);

  m1.addTodo("ekmek al");

  expect(m1.items).toHaveLength(1);

  m1.addTodo("sut al");

  expect(m1.items).toHaveLength(2);

  expect(m1.getVisibleItems()).toHaveLength(2);

  m1.setActiveFilterType(FILTER_TYPES.ACTIVE);
  expect(m1.getVisibleItems()).toHaveLength(2);
  m1.setActiveFilterType(FILTER_TYPES.COMPLETED);
  expect(m1.getVisibleItems()).toHaveLength(0);
  m1.removeTodo("sut al");
  expect(m1.nLeftTodos).toBe(1);
});

it("testing array methods", () => {
  [3, 4, 5].map(function(item, i, arr) {
    expect(i == 0 || i == 1 || i == 2).toBe(true);

    expect([3, 4, 5]).toEqual(arr);
  });

  [3, 4, 5].filter(item => item % 2 === 0).map((item, i, arr) => {
    expect(arr).toEqual([4]);
  });
});
