import React from "react";
import PropTypes from "prop-types";
import TodoAppModel from "./model";
import "./base.css";
import "./index.css";
import TodoAppReducer, { selectors } from "./model";
import Fibaredux from "./fibaredux";
import { createStore, applyMiddleware } from "redux";
import { Provider, connect } from "react-redux";
import DevTools from "./devtools";
class AkilsizTodoHeader extends React.Component {
  render() {
    const { tempTodo, setTempTodo, addTodo } = this.props;

    return (
      <header id="header">
        <h1>todos</h1>
        <form
          onSubmit={e => {
            e.preventDefault();
            addTodo();
          }}
        >
          <input
            id="new-todo"
            value={tempTodo}
            onChange={e => setTempTodo(e.target.value)}
            placeholder="What needs to be done?"
            autoFocus
          />
        </form>
        {/* <pre>{this.state.tempTodo}</pre> */}
      </header>
    );
  }
}

const AkilliTodoHeader = connect(
  function selectState(state) {
    return { tempTodo: state.tempTodo };
  },
  function prepareActionDispatchers(dispatch) {
    return {
      setTempTodo(tempTodo) {
        return dispatch({ type: "SET_TEMP_TODO", tempTodo });
      },
      addTodo: () => dispatch(addTodoMektupCreator())
    };
  }
)(AkilsizTodoHeader);

AkilsizTodoHeader.propTypes = {
  addTodo: PropTypes.func.isRequired
};

function TodoItem(props) {
  return (
    <li>
      <div className="view">
        <input
          checked={props.completed}
          onChange={() => {
            props.toggleTodo(props.description);
          }}
          className="toggle"
          type="checkbox"
        />
        <label>{props.description}</label>
        <button
          className="destroy"
          onClick={() => {
            props.removeTodo(props.description);
          }}
        />
      </div>
      <input className="edit" defaultValue={props.description} />
    </li>
  );
}
class TodoItems extends React.Component {
  render() {
    // const itemEls = [];

    // for (let index = 0; index < items.length; index++) {
    //   const element = items[index];
    //   itemEls.push(
    // <TodoItem
    //   description={element.description}
    //   completed={element.completed}
    // />
    //   );
    // }

    return (
      <ul id="todo-list">
        {this.props.visibleItems.map(todoItem => (
          <TodoItem
            description={todoItem.description}
            completed={todoItem.completed}
            key={todoItem.description}
            toggleTodo={this.props.toggleTodo}
            removeTodo={this.props.removeTodo}
          />
        ))}
      </ul>
    );
  }
}

class TodoFooter extends React.Component {
  render() {
    const { nLeftItems, activeFilter } = this.props;

    return (
      <footer id="footer" style={{ display: "block" }}>
        <span id="todo-count">
          <strong>{nLeftItems}</strong> items left
        </span>
        <ul id="filters">
          <li>
            <a
              className={activeFilter === FILTER_TYPES.ALL ? "selected" : ""}
              onClick={() => this.props.setActiveFilter(FILTER_TYPES.ALL)}
              href="#/"
            >
              All
            </a>
          </li>
          <li>
            <a
              className={activeFilter === FILTER_TYPES.ACTIVE ? "selected" : ""}
              onClick={() => this.props.setActiveFilter(FILTER_TYPES.ACTIVE)}
              href="#/active"
            >
              Active
            </a>
          </li>
          <li>
            <a
              className={
                activeFilter === FILTER_TYPES.COMPLETED ? "selected" : ""
              }
              onClick={() => this.props.setActiveFilter(FILTER_TYPES.COMPLETED)}
              href="#/completed"
            >
              Completed
            </a>
          </li>
        </ul>
      </footer>
    );
  }
}

const FILTER_TYPES = {
  ALL: "ALL",
  COMPLETED: "COMPLETED",
  ACTIVE: "ACTIVE"
};

TodoFooter.propTypes = {
  nLeftItems: PropTypes.number.isRequired,
  activeFilter: PropTypes.oneOf([
    FILTER_TYPES.ACTIVE,
    FILTER_TYPES.ALL,
    FILTER_TYPES.COMPLETED
  ]).isRequired
};
class TodoApp extends React.Component {
  state = {
    items: [
      { description: "ata bak", completed: false },
      { description: "ekmek al", completed: false },
      { description: "sut al", completed: true }
    ],
    activeFilter: FILTER_TYPES.ALL
  };

  getVisibleItems() {
    return this.state.items.filter(element => {
      if (this.state.activeFilter === FILTER_TYPES.ALL) {
        return true;
      }

      if (this.state.activeFilter === FILTER_TYPES.COMPLETED) {
        return element.completed;
      }

      if (this.state.activeFilter === FILTER_TYPES.ACTIVE) {
        return !element.completed;
      }
    });
  }

  toggleTodo(description) {
    // {description: 'x', completed: false}

    // {description: 'x', completed: true}
    const newItems = this.state.items.map(
      item =>
        item.description === description
          ? { description: item.description, completed: !item.completed }
          : item
    );

    this.setState({ items: newItems });
  }

  removeTodo(description) {
    const newItems = this.state.items.filter(
      item => item.description !== description
    );

    this.setState({ items: newItems });
  }

  setActiveFilter(targetFilter) {
    this.setState({ activeFilter: targetFilter });
  }
  render() {
    const { items, activeFilter } = this.state;

    return (
      <section id="todoapp">
        <AkilsizTodoHeader addTodo={this.addTodo.bind(this)} />

        <section id="main" style={{ display: "block" }}>
          <input id="toggle-all" type="checkbox" />
          <label htmlFor="toggle-all">Mark all as complete</label>
          <TodoItems
            visibleItems={this.getVisibleItems()}
            toggleTodo={this.toggleTodo.bind(this)}
            removeTodo={this.removeTodo.bind(this)}
          />
        </section>

        <TodoFooter
          nLeftItems={items.filter(item => !item.completed).length}
          activeFilter={activeFilter}
          setActiveFilter={this.setActiveFilter.bind(this)}
        />
      </section>
    );
  }
}

class TodoAppWithExternalModel extends React.Component {
  render() {
    const {
      activeFilter,
      visibleItems,
      nLeftItems,
      addTodo,
      toggleTodo,
      removeTodo,
      setActiveFilter
    } = this.props;

    return (
      <section id="todoapp">
        <AkilliTodoHeader />

        <section id="main" style={{ display: "block" }}>
          <input id="toggle-all" type="checkbox" />
          <label htmlFor="toggle-all">Mark all as complete</label>
          <TodoItems
            visibleItems={visibleItems}
            toggleTodo={toggleTodo}
            removeTodo={removeTodo}
          />
        </section>

        <TodoFooter
          nLeftItems={nLeftItems}
          activeFilter={activeFilter}
          setActiveFilter={setActiveFilter}
        />
      </section>
    );
  }
}

function addTodoMektupCreator(tempTodo) {
  return {
    type: "ADD_TODO",
    tempTodo
  };
}

const ConnectedTodoApp = connect(
  function(state) {
    return {
      activeFilter: state.activeFilter,
      visibleItems: selectors.getVisibleItems(state),
      nLeftItems: selectors.nLeftTodos(state)
    };
  },
  function(dispatch) {
    return {
      toggleTodo: description => dispatch({ type: "TOGGLE_TODO", description }),
      removeTodo: description => dispatch({ type: "REMOVE_TODO", description }),
      setActiveFilter: targetFilter =>
        dispatch({
          type: "SET_ACTIVE_FILTER_TYPE",
          targetFilter
        })
    };
  }
)(TodoAppWithExternalModel);

// STORE

// const oldDispatch = store.dispatch;

// const newDispatch = function() {
//   console.log("Before dispatch. Received events: ", arguments[0]);
//   console.log("onceki state", store.getState());
//   oldDispatch.apply(store, arguments);
//   console.log("sonraki state", store.getState());

//   console.log("after dispatch");
// };

// store.dispatch = newDispatch.bind(store);

const logger = store => next => action => {
  console.group(action.type);
  console.info("dispatching", action);
  let result = next(action);
  console.log("next state", store.getState());
  console.groupEnd(action.type);
  return result;
};

const store = createStore(
  TodoAppReducer,

  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

store.dispatch({ type: "SET_TEMP_TODO", tempTodo: "ekmek al" });
store.dispatch({ type: "ADD_TODO" });

window.store = store;
export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <div>
          <ConnectedTodoApp />
          <DevTools />
        </div>
      </Provider>
    );
  }
}
