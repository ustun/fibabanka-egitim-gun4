import React, { Component } from "react";
import "./App.css";

const debug = Boolean(window.localStorage.getItem("debug"));

class AkilsizSayac extends React.Component {
  render() {
    return (
      <div>
        {this.props.deger}
        <div>{10 - this.props.deger} daha tiklayiniz</div>
        <button onClick={this.props.artir}>Artir</button>
      </div>
    );
  }
}
class AkilliSayac extends Component {
  state = { sayacDegeri: 0 };

  artir() {
    this.setState({ sayacDegeri: this.state.sayacDegeri + 1 });
  }
  render() {
    return (
      <AkilsizSayac
        deger={this.state.sayacDegeri}
        artir={this.artir.bind(this)}
      />
    );
  }
}

class AkilliSayaclar extends React.Component {
  state = { sayac1: 0, sayac2: 5 };

  sayac1Artir() {
    if (this.state.sayac2 == 0) return;
    this.setState({
      sayac1: this.state.sayac1 + 1,
      sayac2: this.state.sayac2 - 1
    });
  }
  sayac2Artir() {
    if (this.state.sayac1 == 0) return;
    this.setState({
      sayac1: this.state.sayac1 - 1,
      sayac2: this.state.sayac2 + 1
    });
  }
  render() {
    return (
      <div>
        <AkilsizSayac
          deger={this.state.sayac1}
          artir={this.sayac1Artir.bind(this)}
        />
        <AkilsizSayac
          deger={this.state.sayac2}
          artir={this.sayac2Artir.bind(this)}
        />
      </div>
    );
  }
}

class FormOrnegi extends React.Component {
  state = { ad: "", soyad: "", tel: "", acceptTerms: false };
  handleAdOnChange(event) {
    if (/[\d-]/.test(event.target.value)) {
      event.preventDefault();
      return;
    }
    this.setState({ ad: event.target.value });
  }

  handleTelChange(event) {
    if (/[a-z]/.test(event.target.value)) {
      event.preventDefault();
      return;
    }
    this.setState({ tel: event.target.value });
  }

  handleSoyadOnChange(event) {
    this.setState({ soyad: event.target.value });
  }

  handleOnSubmit(event) {
    event.preventDefault();
    console.log("Sending state to server", this.state);
  }

  render() {
    return (
      <form onSubmit={this.handleOnSubmit.bind(this)}>
        <input
          value={this.state.ad}
          placeholder="Ad"
          onChange={this.handleAdOnChange.bind(this)}
        />
        <input
          placeholder="Soyad"
          value={this.state.soyad}
          onChange={this.handleSoyadOnChange.bind(this)}
        />
        <input
          placeholder="tel"
          value={this.state.tel}
          onChange={this.handleTelChange.bind(this)}
        />

        <input
          type="checkbox"
          checked={this.state.acceptTerms}
          onChange={() =>
            this.setState({ acceptTerms: !this.state.acceptTerms })
          }
        />
        <div>
          <div>Adiniz: {this.state.ad.toUpperCase()}</div>
          <div>Soyadiniz: {this.state.soyad.toUpperCase()}</div>
        </div>
        <button>Gonder</button>

        {debug ? <pre>{JSON.stringify(this.state, null, 4)}</pre> : null}
      </form>
    );
  }
}
export default FormOrnegi;
