export default class FibaRedux {
  subscribers = [];
  _state = null;
  constructor(reducer) {
    this.reducer = reducer;
  }
  getState() {
    return this._state;
  }
  subscribe(subscriber) {
    this.subscribers.push(subscriber);
  }
  _informSubscribers() {
    this.subscribers.forEach(subscriber => subscriber());
  }

  dispatch(mektup) {
    this._state = this.reducer(this._state, mektup);
    this._informSubscribers();
  }
}
