import { Map, List } from "immutable";

export const FILTER_TYPES = {
  ALL: "ALL",
  COMPLETED: "COMPLETED",
  ACTIVE: "ACTIVE"
};

function _addTodoHelper(state) {
  if (state.get("items").find(item => item.description === state.tempTodo)) {
    return state;
  }
  const newState = state
    .set("tempTodo", "")
    .set(
      "items",
      state.get("items").push({ description: state.tempTodo, completed: false })
    );

  return newState;
}

function _setActiveFilterHelper(state, activeFilter) {
  return;
}

// it("getters", () => {
//   expect(
//     getters.nLeftTodos({ items: [{ completed: false }, { completed: true }] })
//   ).toBe(1);
// });

export const selectors = {
  nLeftTodos(state) {
    return state.items.filter(item => !item.completed).length;
  },

  getVisibleItems(state) {
    return state.items.filter(element => {
      if (state.activeFilter === FILTER_TYPES.ALL) {
        return true;
      }

      if (state.activeFilter === FILTER_TYPES.COMPLETED) {
        return element.completed;
      }

      if (state.activeFilter === FILTER_TYPES.ACTIVE) {
        return !element.completed;
      }
    });
  }
};

const actionTypes = {
  ADD_TODO: "ADD_TODO",
  SET_ACTIVE_FILTER_TYPE: "SET_ACTIVE_FILTER_TYPE"
};

export default function TodoAppReducer(state, mektup) {
  // Returns new State
  let newState;
  switch (mektup.type) {
    case actionTypes.ADD_TODO: {
      if (state.tempTodo) {
        newState = _addTodoHelper(state);
      } else {
        newState = state;
      }

      break;
    }
    case actionTypes.SET_ACTIVE_FILTER_TYPE: {
      return state.set("activeFilter", activeFilter);
    }

    case "SET_TEMP_TODO":
      return state.set("tempTodo", mektup.tempTodo);
    case "REMOVE_TODO": {
      return state.update("items", oldItems =>
        oldItems.filter(item => item.description != mektup.description)
      );
    }
    case "TOGGLE_TODO":
      newState = {
        ...state,
        items: state.items.map(
          item =>
            item.description != mektup.description
              ? item
              : { ...item, completed: !item.completed }
        )
      };
      break;
    //   );
    default:
      newState = Map({
        items: List(),
        activeFilter: FILTER_TYPES.ALL,
        tempTodo: ""
      });
  }

  return newState;
}
