class Person {
  constructor(public id: number, public name: string) {}

  setName(name) {
    return new Person(this.id, name);
  }
}

var p = new Person(1, "Ali");

var p2 = p.setName("Veli");
