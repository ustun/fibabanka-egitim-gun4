import { Map, List, Record, fromJS } from "immutable";

it("test map methods", () => {
  {
    const m = new Map({ name: "Ali", surname: "Ozgur", id: 123 });

    expect(m.get("name"), "Ali");

    // m.name = "veli"

    // {...state, name: 'Ustun'}

    const m2 = m.set("name", "Veli");

    expect(m.get("name"), "Ali");

    expect(m2.get("name"), "Veli");

    expect(m2.get("surname"), "Ozgur");

    const l1 = new List();

    expect(l1.count()).toBe(0);

    const l2 = l1.push(3);
    expect(l1.count()).toBe(0);
    expect(l2.count()).toBe(1);
  }

  let ali = fromJS({ name: "Ali", hobbies: ["Futbol", "Muzik"] });

  expect(ali.get("hobbies").count()).toBe(2);

  ali = ali.set("hobbies", ali.get("hobbies").push("motosiklet"));

  expect(ali.get("hobbies").count()).toBe(3);

  ali = ali.update("hobbies", old => old.push("motosiklet"));

  // getIn , setIn
  const x = fromJS({ a: { b: { c: 2 } } });

  // x.a.b.c = 3

  expect(x.getIn(["a", "b", "c"])).toBe(2);

  const x2 = x.setIn(["a", "b", "c"], 32);

  expect(x2.getIn(["a", "b", "c"])).toBe(32);

  const x3 = x2.updateIn(["a", "b", "c"], x => x + 4);

  expect(x3.getIn(["a", "b", "c"])).toBe(36);

  // updateIn

  const Insan = Record({ ad: "", soyad: "Veli" });

  const r1 = new Insan({ ad: "Ali" });

  expect(r1.ad).toBe("Ali");
  expect(r1.soyad).toBe("Veli");

  expect(() => {
    r1.set("id", 12);
  }).toThrow();
});
